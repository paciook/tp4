def perimetroAreaCirculo(radio):
    try:
        if radio <= 0:
            print("Valor no válido")
            return -1
        perimetro = 3.14159 * radio
        area = 3.14159 * radio * radio
        print("El perímetro del circulo es " + str(perimetro))
        print("Y su area es " + str(area))
        resultado = (area, perimetro)
        return resultado
    except:
        print("Error xd")
        return -1
if __name__ == "__main__":
    radio = int(input("Ingrese el radio del circulo "))

    perimetroAreaCirculo(radio)
