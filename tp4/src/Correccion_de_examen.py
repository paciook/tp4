def correcion():
    """La funcion le solicita al usuario la cantidad de ejercicios que tiene el
       examen y luego solicita la cantidad de
       ejercicios bien hechos por el alumno."""

    while True:
        try:
            total = float(input("¿Cuantos ejercicios tiene el examen? "))
            aprobacion = float(input("¿Cuanto es el " +
                                     "porcentaje para aprobar? "))
            break
        except ValueError:
            print("Valor no valido")
    # Se le pide al usuario la cantidad total de ejercicios y el porcentaje
    # de aprobacion

    listaAlumnos = []
    bienHecho = 0

    while True:   # Utilizo un "while True" para hacerlo hasta la interrupcion
        try:
            bienHecho = int(input("¿Cuantos hizo bien" +
                                  " el alumno? ('*' para terminar) "))
        except ValueError:
            print("Valor no valido")
        # Se le pide al usuario la cantidad de
        # ejercicios bien hechos por alumno

        if bienHecho == "*":  # Valor de interrupcion
            for x in range(1, len(listaAlumnos)):
                print(x, "- ", listaAlumnos[x - 1])
            break
        elif float(bienHecho) >= 0 and float(bienHecho) <= total:

            # Se revisa si la cantidad bien hecha es menor al maximo

            nota = 100 / total * float(bienHecho)

            # Se calcula el porcentaje

            if nota >= aprobacion:
                listaAlumnos.append(nota)
                print(nota, " Aprobado")
            else:
                listaAlumnos.append(nota)
                print(nota, " Desaprobado")

            # Se imprime la condicion correspondiente

        else:
            print("valor no valido")

        # Si se ingresa un caracter no validado por las
        # condiciones ya descritas se muestra una leyenda
        # que lo indique
if __name__ == "__main__":
    correcion()
