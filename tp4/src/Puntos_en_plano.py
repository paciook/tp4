import math


def distanciaAlOrigen(x1, x2, x3):
    """El programa recibe 3 puntos en un plano y verifica cual esta mas lejos del
       eje de coordenadas y lo indica"""
    dist1 = float(math.sqrt((x1[0]**2 + x1[1]**2)))
    dist3 = float(math.sqrt((x3[0]**2 + x3[1]**2)))
    dist2 = float(math.sqrt((x2[0]**2 + x2[1]**2)))
    masGrande = []
    distancias = [dist1, dist2, dist3]
    distancias.sort()
    if dist1 == distancias[-1]:
        masGrande.append("X1")
    if dist2 == distancias[-1]:
        masGrande.append("X2")
    if dist3 == distancias[-1]:
        masGrande.append("X3")
    if len(masGrande) == 1:
        print("El punto mas lejano al eje de coordenadas es: ", masGrande)
    else:
        print("Los puntos mas lejanos al eje de coordenadas son: ", masGrande)

# "distanciaAlOrigen" calcula la distancia de los 3 puntos dados
# recibidos como parametros hacia el eje de coordenadas y devuelve una
# leyenda que indique cual es el que esta mas lejos
if __name__ == "__main__":
    try:
        x1 = [int(input("Ingrese valor en Y de X1 ")),
              int(input("Ingrese valor en X de X1 "))]
        x2 = [int(input("Ingrese valor en Y de X2 ")),
              int(input("Ingrese valor en X de X2 "))]
        x3 = [int(input("Ingrese valor en Y de X3 ")),
              int(input("Ingrese valor en X de X3 "))]
        distanciaAlOrigen(x1, x2, x3)
    except ValueError:
        print("Valor invalido, empeza de 0 papi")
