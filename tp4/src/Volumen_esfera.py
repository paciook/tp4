def volumenEsfera(radio):
    try:
        if radio > 0:

            volumen = 0.75 * 3.14159 * radio * radio * radio
            print("El volúmen de la esfera es " + str(volumen))
            return volumen
        else:
            print("Valor no válido")
            return -1
    except:
        print("Error xd")
        return -1

if __name__ == "__main__":
    radio = float(input("Ingrese el radio de la esfera "))
    volumenEsfera(radio)
