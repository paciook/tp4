def area(base, altura):
    if not isinstance(base, (int, float)) or not isinstance(altura, (int, float)):
        print("Por hoy no funcionamos con letras o simbolos")
        return -1
    if base <= 0 or altura <= 0:
        print("Valor no valido")
        return -1
    area = base * altura
    print("El área del rectángulo es " + str(area))
    return area

if __name__ == "__main__":
    base = int(input("Ingrese base del rectángulo "))
    altura = int(input("Ingrese altura del rectángulo "))

    area(base, altura)
