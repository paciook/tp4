def matrizIdentidad(n):
    """El programa le pide una dimensión al usuario y llama a una funcion
       para que ésta dibuje la matriz identidad de esa dimensión"""
    if not isinstance(n, (int, float)) or n < 1:
        print("Valor incorrecto")
        return -1
    cadena = ""
    devolucion = [n]
    for fila in range(n):  # Este "for" se mueve verticalmente en la matriz
        for columna in range(n):  # Este "for" se mueve horizontalmente
            if columna == fila:               # Si la columna y la fila
                cadena = str(cadena + "\t1")  # coinciden, se escribe un 1,
                devolucion.append(1)
            else:                             # si no, se escribe un 0.
                cadena = str(cadena + "\t0")  # (todo esto se guarda en una
                devolucion.append(0)
        print(cadena)             # variable llamada "cadena")
        cadena = ""
        return devolucion

# La funcion es llamada con un parametro pedido al usuario
# si el parametro no es numerico,
# se muestra en pantalla el error
if __name__ == "__main__":
    try:
        matrizIdentidad(int(input("Ingrese una dimensión natural: ")))
    except ValueError:
        print("Valor no valido, tiene que ser numerico")
