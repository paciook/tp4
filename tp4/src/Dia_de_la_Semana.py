
# La declaración del diccionario está adentro de
# la función porque ya lo charlamos, Fede.


def queDia(dia):
    """La funcion pide el dia en un año, y devuelve el dia de la
       semana que es ese dia. En el caso de algún error, la función
       devuelve el valor -1"""
    try:
        if dia <= 0 or dia > 366:
            print("Dia no válido")
            return -1

        numero_a_dia = {0: "Domingo",
                        1: "Lunes",
                        2: "Martes",
                        3: "Miercoles",
                        4: "Jueves",
                        5: "Viernes",
                        6: "Sabado"}

        nombreDia = dia % 7
        print(numero_a_dia[nombreDia])
        return numero_a_dia[nombreDia]
    except:
        print("Error xd")
        return -1


if __name__ == "__main__":
    dia = input("Ingrese un dia del año: ")
    try:
        queDia(int(dia))
    except ValueError:
        print("Dia o mes no numerico")
