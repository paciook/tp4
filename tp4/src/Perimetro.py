def perimetro(base, altura):
    try:
        if base <= 0 or altura <= 0:
            print("Valor no válido")
            return -1
        perimetro = (2 * base) + (2 * altura)
        print("El perimetro del rectangulo es " + str(perimetro))
        return perimetro
    except:
        print("Error xd")
        return -1

if __name__ == "__main__":
    base = int(input("Ingrese base del rectangulo "))
    altura = int(input("Ingrese altura del rectangulo "))

    perimetro(base, altura)
