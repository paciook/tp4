def repiteX1000(palabra):
    """El programa le pide al usuario una palabra y la imprime 1000 veces"""
    devolucion = []
    for x in range(1000):
        devolucion.append(palabra)
        print(palabra, end=" ")
    print("")
    return devolucion
# "repiteX1000" recibe el parametro de 'palabra' y la imprime 1000 veces
if __name__ == "__main__":
    repiteX1000(str(input("Ingrese una palabra: ")))
