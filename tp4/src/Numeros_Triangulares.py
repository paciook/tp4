import sys
valores = []


def imprimeTriangulares(n, Lista=[], x=0):
    """El programa le pide al usuario un numero y le devuelve una \
       lista de los numeros triangulares hasta el numero requerido inclusive"""

    # La excepcion de n == 1 sirve para destrabar
    # las demas llamadas a la funcion
    if x == 0:
        valores.clear()
    try:
        if n == 1:
            print(str(n), "- ", str(n))
            valores.append(1)
            recursividad = 1
            devolucion = [recursividad, valores]
            return devolucion
        else:
            recursividad = (n + (imprimeTriangulares(n - 1, valores, 1)[0]))
            print(str(n), "- ", str(recursividad))
            valores.append(recursividad)
            devolucion = [recursividad, valores]
            return devolucion
    except:
        print("Valor no válido")
        return -1
# Esta expcecion llama a la funcion con un valor inferior
# a la que haya sido llamada por ultima vez y lo suma
# a la variable recursividad por donde pasan los numeros
# triangulares calculados

# Setea el limite de recursion para aumentar
# el numero maximo con el que se puede operar
sys.setrecursionlimit(10000)
# Llamado a la funcion con un parametro pedido al usuario
if __name__ == "__main__":
    try:
        imprimeTriangulares(int(input("Ingrese n: ")), valores)
    except ValueError:
        print("Valor invalido")
