def imprimeDomino():
    """La funcion imprime las fichas del domino, no recibe ni devuelve nada"""
    devolucion = []
    for izquierda in range(7):                          # La funcion utiliza
        print(str(izquierda) + " | " + str(izquierda))  # el numero de la
        devolucion.append(str(izquierda) + " " + str(izquierda))
        if izquierda == 6:                              # izquierda e imprime
            return devolucion                           # desde ese numero
        for derecha in range(izquierda, 7):             # hasta el 6 en la
            if izquierda != derecha:                    # derecha
                print(str(izquierda) + " | " + str(derecha))
                devolucion.append(str(izquierda) + " " + str(derecha))
if __name__ == "__main__":
    imprimeDomino()
