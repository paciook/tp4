

def numerosRomanos(arabigo):
    """El programa le pide al usuario un numero arabigo
       y devuelve dicho numero en numeros romanos"""

    # Declaracion de diccionarios
    try:
        if arabigo <= 0 or arabigo > 1000000:
            print("Valor inválido")
            return -1
    except:
        print("Sin letras, campeón")
        return -1

    unidad = {
                1: "I",
                2: "II",
                3: "III",
                4: "IV",
                5: "V",
                6: "VI",
                7: "VII",
                8: "VIII",
                9: "IX"
    }
    decena = {
                1: "X",
                2: "XX",
                3: "XXX",
                4: "XL",
                5: "L",
                6: "LX",
                7: "LXX",
                8: "LXXX",
                9: "XC"
    }
    centena = {
                1: "C",
                2: "CC",
                3: "CCC",
                4: "CD",
                5: "D",
                6: "DC",
                7: "DCC",
                8: "DCC",
                9: "CM"
    }
    unidad_de_mil = {
                        1: "M",
                        2: "MM",
                        3: "MMM",
                        4: u"I\u0305V\u0305",
                        5: u"V\u0305",
                        6: u"V\u0305I\u0305",
                        7: u"V\u0305I\u0305I\u0305",
                        8: u"V\u0305I\u0305I\u0305I\u0305",
                        9: u"I\u0305X\u0305"
    }
    decena_de_mil = {
                        1: u"X\u0305",
                        2: u"X\u0305X\u0305",
                        3: u"X\u0305X\u0305X\u0305",
                        4: u"X\u0305L\u0305",
                        5: u"L\u0305",
                        6: u"L\u0305X\u0305",
                        7: u"L\u0305X\u0305X\u0305",
                        8: u"L\u0305X\u0305X\u0305X\u0305",
                        9: u"X\u0305C\u0305"
    }
    centena_de_mil = {
                        1: "C\u0305",
                        2: "C\u0305C\u0305",
                        3: "C\u0305C\u0305C\u0305",
                        4: "C\u0305D\u0305",
                        5: "D\u0305",
                        6: "D\u0305C\u0305",
                        7: "D\u0305C\u0305C\u0305",
                        8: "D\u0305C\u0305C\u0305C\u0305",
                        9: "C\u0305M\u0305"
    }
    unidad_de_millon = {1: u"M\u0305"}

    # Encapsulamiento de diccionarios
    diccionarios = (unidad, decena, centena, unidad_de_mil,
                    decena_de_mil, centena_de_mil, unidad_de_millon)

    # La funcion recibe dos parametros: el numero a transformar y los
    # diccionarios. Desencapsula los diccionarios y revisa la cantidad
    # de unidades, decenas, centenas... que hay en el número a transformar.
    # Concatena los caracteres correspondientes y los imprime en pantalla.
    
    # Calculo de cantidad de cada una de las unidades
    uni = int(arabigo) % 10
    dec = (int(arabigo) // 10) % 10
    cent = (int(arabigo) // 100) % 10
    uniDeMil = (int(arabigo) // 1000) % 10
    decDeMil = (int(arabigo) // 10000) % 10
    centDeMil = (int(arabigo) // 100000) % 10
    uniDeMillon = int(arabigo) // 1000000
    resultado = ""

    # Concatenacion de digitos segun si tienen una llave en el diccionario o no
    if uniDeMillon in unidad_de_millon:
        resultado = unidad_de_millon[uniDeMillon]
    if centDeMil in centena_de_mil:
        resultado += centena_de_mil[centDeMil]
    if decDeMil in decena_de_mil:
        resultado += decena_de_mil[decDeMil]
    if uniDeMil in unidad_de_mil:
        resultado += unidad_de_mil[uniDeMil]
    if cent in centena:
        resultado += centena[cent]
    if dec in decena:
        resultado += decena[dec]
    if uni in unidad:
        resultado += unidad[uni]
    print(resultado)
    return resultado

# Llamado a la funcion con el parametro pedido al
# usuario y los diccionarios encapsulados
if __name__ == "__main__":
    try:
        arabigo = int(input("Ingrese un numero arabigo: "))
        if arabigo < 1000001 and arabigo > 0:
            numerosRomanos(arabigo)
        else:
            print("Valor no valido")
    except ValueError:
        print("Ingrese valor numerico")
