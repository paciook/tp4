import math


def vertices(a, b, c):
    """La funcion recibe los parametros a, b, c y devuelve el vertice"""
    devolucion = []
    if a == 0:
        print("Ingrese una funcion cuadratica")
        return
    x = -(b) / (2 * a)
    y = (a * (x**2)) + (b * x) + c
    if (a * ((x + 1)**2)) + (b * (x + 1)) + c > y:
        print("Vertice: ", "(", x, ":", y, ") -> Es un minimo")
        devolucion.append(str(x) + " " + str(y))
        return devolucion
    else:
        print("Vertice: ", "(", x, ":", y, ") -> Es un maximo")
        devolucion.append(str(x) + " " + str(y))
        return devolucion
# "veritces()" recibe los parametros 'a', 'b', 'c' y con ellos
# calcula el vertice de la funcion cuadratica haciendo -b/2a
# y determina si es un minimo o un maximo comparando con un valor
# de 'x' mayor. Devuelve el vertice e indica si es maximo o minimo


def raices(a, b, c):
    """La funcion recibe los parametros a, b, c y devuelve las raices"""
    if a == 0:
        print("Ingrese una funcion cuadratica")
        return
    x = [0, 0, ""]
    if (b ** 2) - (4 * a * c) < 0:
        discriminante = -((b ** 2) - (4 * a * c))
        x[2] = "i"
    else:
        discriminante = (b ** 2) - (4 * a * c)

    x[0] = (-(b) + math.sqrt(discriminante)) / (2 * a)
    x[1] = (-(b) - math.sqrt(discriminante)) / (2 * a)
    if discriminante == 0:
        print("Raiz: ", "{", x[0], "}")
        return "Raiz: ", "{", x[0], "}"
    else:
        print("Raices: ", "{", str(x[0]), x[2], " , ", str(x[1]), x[2], "}")
        return "Raices: ", "{", str(x[0]), x[2], " , ", str(x[1]), x[2], "}"
# "raices" recibe los parametros 'a', 'b', 'c' y con ellos calcula
# las raices usando la resolvente. Devuelve las raices o la raiz


def interseccion(recta1, recta2):
    """La funcion recibe dos listas como parametros que contienen los datos
       de las dos rectas y devuelve el punto de interseccion"""
    if recta1[0] == recta2[0]:
        if recta1[1] == recta2[1]:
            print("Interseccion en todo el dominio")
            return "Interseccion en todo el dominio"
        else:
            print("Interseccion vacia")
            return "Interseccion vacia"
    x = (recta2[1] - recta1[1]) / (recta1[0] - recta2[0])
    y = (x * recta1[0]) + recta1[1]
    base = "({} : {})"
    print(base.format(x, y))
    return base.format(x, y)

# "interseccion" recibe dos parametros que son listas, cada una
# correspondiente a una funcion distina. Calcula el valor en el que
# hay una interseccion y devuelve ese punto o conjunto
if __name__ == "__main__":
    queFuncion = input("Qué funcion querés?. Raices y vertice = 1," +
                       " Interseccion = 2 ")
    if int(queFuncion) == 1:
        a, b, c = input("Ingrese a, b y c separados por un espacio ").split()
        try:
            vertices(float(a), float(b), float(c))
            raices(float(a), float(b), float(c))
        except ValueError:
            print("Error al recibir los valores")
    elif int(queFuncion) == 2:
        recta1 = []
        recta2 = []
        a, b = input("Ingrese a y b separados" +
                     " por un espacio (recta 1) ").split()
        a, b = float(a), float(b)
        recta1.append(a)
        recta1.append(b)
        a, b = input("Ingrese a y b separados" +
                     " por un espacio (recta 2) ").split()
        a, b = float(a), float(b)
        recta2.append(a)
        recta2.append(b)
        try:
            interseccion(recta1, recta2)
        except ValueError:
            print("Valores no validos")
