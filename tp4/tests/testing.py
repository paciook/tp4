import unittest
import sys
import os
from os import path
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import src.Area as Area  # noqa
import src.Area_con_coordenadas as Area_con_coordenadas  # noqa
import src.Correccion_de_examen as Correccion_de_examen  # noqa
import src.Dia_de_la_Semana as Dia_de_la_Semana  # noqa
import src.Domino as Domino  # noqa
import src.Funciones_Matematicas as Funciones_Matematicas  # noqa
import src.Matriz_identidad as Matriz_identidad  # noqa
import src.Numeros_Romanos as Numeros_Romanos  # noqa
import src.Numeros_Triangulares as Numeros_Triangulares  # noqa
import src.Perimetro_Area_Circulo as PACirculo  # noqa
import src.Perimetro as Perimetro  # noqa
import src.Puntos_en_plano as Puntos_en_plano  # noqa
import src.Repite_x_1000 as Repite_x_1000  # noqa
import src.Volumen_esfera as Volumen_esfera  # noqa


class TestArea(unittest.TestCase):
    def test_area(self):
        self.assertEqual(Area.area(2, 2), 4)
        self.assertEqual(Area.area(-2, 7), -1)
        self.assertEqual(Area.area(15, -6), -1)
        self.assertEqual(Area.area(-1, -8), -1)
        self.assertEqual(Area.area(12, 'asasf'), -1)
        self.assertEqual(Area.area('asasf', 'asdasf'), -1)
        self.assertEqual(Area.area('afdasdf', 'asdfasd'), -1)


class TestArea_con_coordenadas(unittest.TestCase):
    def test_area(self):

        # xx = [y, x]

        # Figura irregular
        x1 = [4, 5]
        x2 = [2, 8]
        x3 = [4, 2]
        x4 = [4, 8]
        self.assertEqual(Area_con_coordenadas.area(x1, x2, x3, x4), -1)

        # Por debajo del eje de coordenadas

        x1 = [-4, 4]
        x2 = [-2, 4]
        x3 = [-2, 9]
        x4 = [-4, 9]
        self.assertEqual(Area_con_coordenadas.area(x1, x2, x3, x4), 10)

        # Y invertida

        x1 = [4, 4]
        x2 = [2, 4]
        x3 = [2, 9]
        x4 = [4, 9]
        self.assertEqual(Area_con_coordenadas.area(x1, x2, x3, x4), -1)

        # Normal

        x1 = [2, 4]
        x2 = [4, 4]
        x3 = [4, 9]
        x4 = [2, 9]
        self.assertEqual(Area_con_coordenadas.area(x1, x2, x3, x4), 10)

        # Letras

        x1 = ["a", 4]
        x2 = [4, "b"]
        x3 = [4, 9]
        x4 = ["C", 9]
        self.assertEqual(Area_con_coordenadas.area(x1, x2, x3, x4), -1)


class TestCorrecion_de_examen(unittest.TestCase):
    pass


class TestDia_de_la_semana(unittest.TestCase):
    def test_queDia(self):
        self.assertEqual(Dia_de_la_Semana.queDia(5), "Viernes")
        self.assertEqual(Dia_de_la_Semana.queDia("as"), -1)
        self.assertEqual(Dia_de_la_Semana.queDia(-4), -1)
        self.assertEqual(Dia_de_la_Semana.queDia(544), -1)


class TestDomino(unittest.TestCase):
    def test_Domino(self):
        self.assertEqual(len(Domino.imprimeDomino()), 28)


class TestFunciones_Matematicas(unittest.TestCase):
    def test_Funciones_Matematicas(self):
        # Vertices

        self.assertEqual(Funciones_Matematicas.vertices(4, 1, 5)[0],
                         "-0.125 4.9375")
        self.assertEqual(Funciones_Matematicas.vertices("a", 5, "b"), -1)

        # Raices

        self.assertEqual(Funciones_Matematicas.raices(-1, 4, 2)[0],
                         "-0.4494897427831779 , 4.449489742783178")
        self.assertEqual(Funciones_Matematicas.raices("a", 5, "b"), -1)

        # Interseccion
        self.assertEqual(Funciones_Matematicas.interseccion([2, 5], [4, 2]),
                         "1.5 8.0")
        self.assertEqual(Funciones_Matematicas.interseccion([2, 4], [2, 9]),
                         -2)
        self.assertEqual(Funciones_Matematicas.interseccion(["a", 5], [6, "b"]),
                         -1)


class TestMatriz_identidad(unittest.TestCase):
    def test_Matriz_identidad(self):
        self.assertEqual(Matriz_identidad.matrizIdentidad(5)[0], 5)
        self.assertEqual(Matriz_identidad.matrizIdentidad("d"), -1)
        self.assertEqual(Matriz_identidad.matrizIdentidad(-5), -1)


class TestNumeros_Romanos(unittest.TestCase):
    def test_Numeros_Romanos(self):
        self.assertEqual(Numeros_Romanos.numerosRomanos(105), "CV")
        self.assertEqual(Numeros_Romanos.numerosRomanos(1000000), u"M\u0305")
        self.assertEqual(Numeros_Romanos.numerosRomanos(0), -1)
        self.assertEqual(Numeros_Romanos.numerosRomanos("adf"), -1)
        self.assertEqual(Numeros_Romanos.numerosRomanos(-5), -1)
        self.assertEqual(Numeros_Romanos.numerosRomanos(1554981515), -1)


class TestNumeros_Triangulares(unittest.TestCase):
    def test_Numeros_Triangulares(self):
        self.assertEqual(Numeros_Triangulares.imprimeTriangulares(5)[0], 15)
        self.assertEqual(len(Numeros_Triangulares.imprimeTriangulares(5)[1]),
                         5)
        self.assertEqual(Numeros_Triangulares.imprimeTriangulares("ass"), -1)


class TestPerimetro_Area_Circulo(unittest.TestCase):
    def test_Perimetro_Area_Circulo(self):
        self.assertEqual(PACirculo.perimetroAreaCirculo(2),
                                                       (12.56636, 6.28318))
        self.assertEqual(PACirculo.perimetroAreaCirculo("sad"), -1)
        self.assertEqual(PACirculo.perimetroAreaCirculo(-4), -1)


class TestPerimetro(unittest.TestCase):
    def test_Perimetro(self):
        self.assertEqual(Perimetro.perimetro("asda", 2), -1)
        self.assertEqual(Perimetro.perimetro(5, 3), 16)
        self.assertEqual(Perimetro.perimetro(-5, -8), -1)


class TestPuntos_en_plano(unittest.TestCase):
    pass


class TestRepite_x_1000(unittest.TestCase):
    def test_Repite_x_1000(self):
        self.assertEqual(len(Repite_x_1000.repiteX1000("Windows")), 1000)
        self.assertEqual(Repite_x_1000.repiteX1000("Windows")[0], "Windows")


class TestVolumen_esfera(unittest.TestCase):
    def test_Volumen_esfera(self):
        self.assertEqual(Volumen_esfera.volumenEsfera(87), 1551559.8298274998)
        self.assertEqual(Volumen_esfera.volumenEsfera("asda"), -1)
        self.assertEqual(Volumen_esfera.volumenEsfera(-5), -1)
        self.assertEqual(Volumen_esfera.volumenEsfera(0), -1)


if __name__ == "__main__":
    unittest.main()
